﻿using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace MpiDataSyncApi.Services
{
    public class NetworkService
    {
        private static NetworkService _networkService;

        public NetworkService()
        {

        }

        public static NetworkService Instance
        {
            get
            {
                if (_networkService == null)
                {
                    _networkService = new NetworkService();
                }
                return _networkService;
            }
        }

        public async Task<IPAddress> GetLocalIpAddress()
        {
            var hostEntrey = await Dns.GetHostEntryAsync(Dns.GetHostName());
            return hostEntrey.AddressList.FirstOrDefault(f => f.AddressFamily == AddressFamily.InterNetwork);
        }

        public async Task<string> GetMacAddress()
        {
            var macAddresses =
            (
                from nic in NetworkInterface.GetAllNetworkInterfaces()
                where nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                select nic.GetPhysicalAddress().ToString()
            );
            return macAddresses.FirstOrDefault();
        }

        public async Task<string> GetHostName()
        {
            return Dns.GetHostName();
        }
    }
}