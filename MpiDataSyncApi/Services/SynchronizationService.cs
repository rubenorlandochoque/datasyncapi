﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MpiDataSyncApi.Controllers;
using MpiDataSyncApi.Models.Settings;
using MpiDataSyncApi.Models.Surveys.Dto;
using MpiDataSyncApi.Models.Synchronizations.Dto;
using Newtonsoft.Json;
using File = MpiDataSyncApi.Models.Files.File;

namespace MpiDataSyncApi.Services
{
    public class SynchronizationService
    {
        private static SynchronizationService _synchronizationService;
        //private string _defaultSyncApiBaseUri = "http://localhost:8011/WebAPI/";
        private string _defaultSyncApiBaseUri = "http://apisurvey-apisurveyquality.azurewebsites.net/";
        private string _syncApiBaseUri;
        private readonly HttpClient _httpClient;

        public SynchronizationService(ISettingRepository settingRepository, string name)
        {
            _httpClient = new HttpClient
            {
                MaxResponseContentBufferSize = 1024 * 1024 * 1024, // 1024Mb = 1Gb
                Timeout = TimeSpan.FromDays(7)
            };
            UpdateUrl(settingRepository, name);
        }

        private void UpdateUrl(ISettingRepository settingRepository, string name)
        {
            var defaultSyncApiUri = settingRepository.GetByKeyAndName("SYNC_API_BASE_URI", name);
            _syncApiBaseUri = defaultSyncApiUri?.Value ?? _defaultSyncApiBaseUri;
        }

        public static SynchronizationService GetInstance(ISettingRepository settingRepository, string name)
        {
            if (_synchronizationService == null)
            {
                _synchronizationService = new SynchronizationService(settingRepository, name);
            }
            _synchronizationService.UpdateUrl(settingRepository, name);
            return _synchronizationService;
        }

        public async Task<bool> UploadFile(string extractedFilesBaseDirectory, string deviceCode, string extractionCode, string formCode, string versionCode, File file, ILogger<BroadcasterController> logger)
        {
            //try
            //{
            string filePath = extractedFilesBaseDirectory + $@"\{deviceCode}\{extractionCode}\{file.Filename}";

            if (!System.IO.File.Exists(filePath) && !string.IsNullOrEmpty(file.Filename))
            {
                // Quiere decir que NO es una foto NEGADA, AUSENTE ni IMPOSIBILITADA y físicamente NO EXISTE
                //logger.LogError($"SynchronizationService > UploadFile > Error: El archivo \"{filePath}\" NO EXISTE");
                return false;
            }

            MultipartFormDataContent formData = new MultipartFormDataContent();

            if (System.IO.File.Exists(filePath))
            {
                FileStream fs = System.IO.File.OpenRead(filePath);
                HttpContent streamContent = new StreamContent(fs);
                HttpContent fileContent = new ByteArrayContent(streamContent.ReadAsByteArrayAsync().Result);
                fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                formData.Add(fileContent, "file", Path.GetFileName(filePath));
            }

            HttpContent applicationContent = new StringContent("relevamiento");
            formData.Add(applicationContent, "application");

            HttpContent formCodeContent = new StringContent(formCode);
            formData.Add(formCodeContent, "formCode");

            HttpContent versionCodeContent = new StringContent(versionCode);
            formData.Add(versionCodeContent, "versionCode");

            HttpContent entityCodeContent = new StringContent(file.EntityCode);
            formData.Add(entityCodeContent, "entityCode");

            HttpContent objectCodeContent = new StringContent(file.ObjectCode);
            formData.Add(objectCodeContent, "objectCode");

            HttpContent surveyCodeContent = new StringContent(file.SurveyCode);
            formData.Add(surveyCodeContent, "surveyCode");

            HttpContent durationContent = new StringContent(file.Duration.ToString());
            formData.Add(durationContent, "duration");

            HttpContent filenameContent = new StringContent(file.Filename);
            formData.Add(filenameContent, "filename");

            HttpContent registerDateContent = new StringContent(file.RegisterDate);
            formData.Add(registerDateContent, "registerDate");

            HttpContent observationTypeCodeContent = new StringContent(file.ObservationTypeCode);
            formData.Add(observationTypeCodeContent, "observationTypeCode");

            HttpContent fileClasificationCodeContent = new StringContent(file.FileClasificationCode);
            formData.Add(fileClasificationCodeContent, "fileClasificationCode");

            HttpContent questionSectionCodeContent = new StringContent(file.QuestionSectionCode);
            formData.Add(questionSectionCodeContent, "questionSectionCode");

            HttpContent multimediaAuxCodeContent = new StringContent(file.MultimediaCode);
            formData.Add(multimediaAuxCodeContent, "multimediaAuxCode");

            HttpContent groupingContent = new StringContent(file.GroupCode);
            formData.Add(groupingContent, "grouping");

            var response = await _httpClient.PostAsync(_syncApiBaseUri + "api/multimedia/relevamiento/upload", formData);
            logger.LogError($"SynchronizationService > UploadFile > Respuesta SurveyApi: {response.StatusCode} - {response.IsSuccessStatusCode} - {file.MultimediaCode}");
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            var jsonStr = await response.Content.ReadAsStringAsync();
            //logger.LogError("SynchronizationService > UploadFile > jsonStr: " + jsonStr);
            var responseStr = JsonConvert.SerializeObject(response, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            //logger.LogError("SynchronizationService > UploadFile > responseStr: " + responseStr);
            return false;
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}
        }

        public async Task<bool> UploadSynchronizationLog(LogDto logDto)
        {
            var uri = new Uri(_syncApiBaseUri + "api/synclog/Save");
            HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(logDto), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(uri, contentPost);
            if (response.IsSuccessStatusCode)
            {
                var jsonStr = await response.Content.ReadAsStringAsync();
                var json = JsonConvert.DeserializeObject<dynamic>(jsonStr);
                int resultCode = json["ResultCode"];
                if (resultCode == 1)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public async Task<bool> UploadSurvey(UploadSurveyDto uploadSurveyDto)
        {
            var uri = new Uri(_syncApiBaseUri + "api/survey/upload");
            HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(uploadSurveyDto), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(uri, contentPost);
            if (response.IsSuccessStatusCode)
            {
                //var jsonStr = await response.Content.ReadAsStringAsync();
                return true;
            }
            return false;
        }
    }
}
