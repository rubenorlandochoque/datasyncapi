﻿using System;
using Microsoft.Data.Sqlite;

namespace MpiDataSyncApi.Services
{
    public class SQLiteService
    {
        private static SQLiteService _instance;
        private SqliteConnection _sqliteConnection;

        public SQLiteService()
        {
        }

        //public static SQLiteService Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //        {
        //            _instance = new SQLiteService();
        //        }
        //        return _instance;
        //    }
        //}

        public void OpenConnection(string extractedFilesBaseDirectory, string deviceCode, string extractionCode)
        {
            if (_sqliteConnection != null)
            {
                _sqliteConnection.Close();
                _sqliteConnection = null;
            }
            try
            {
                string connectionString = $@"{extractedFilesBaseDirectory}\{deviceCode}\{extractionCode}\dc.sqlite";
                _sqliteConnection = new SqliteConnection("" +
                new SqliteConnectionStringBuilder
                {
                    DataSource = connectionString
                });
                _sqliteConnection.Open();
            }
            catch (Exception ex)
            {
                _sqliteConnection = null;
            }
        }

        public void CloseConnection()
        {
            if (_sqliteConnection == null) return;
            _sqliteConnection.Close();
            _sqliteConnection = null;
        }

        public string GetApplicationCode(string formCode)
        {
            string applicationCode = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                //var insertCommand = _sqliteConnection.CreateCommand();
                //insertCommand.Transaction = transaction;
                //insertCommand.CommandText = "INSERT INTO message ( text ) VALUES ( $text )";
                //insertCommand.Parameters.AddWithValue("$text", "Hello, World!");
                //insertCommand.ExecuteNonQuery();

                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = $"SELECT f.CodSistema FROM formularios AS f WHERE f.CodFormulario = '{formCode}';";
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        applicationCode = reader.GetString(0);
                        Console.WriteLine(applicationCode);
                    }
                }

                transaction.Commit();
            }
            return applicationCode;
        }

        /// <summary>
        /// Encabezado (getEncabezado)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetHeader(string versionCode, string surveyCode)
        {
            string header = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(\n" +
                                           "    case when f.CodSistema  is null then '' else f.CodSistema end  || '·' || \n" +
                                           "    case when e.CodVersion  is null then '' else e.CodVersion end  || '·' || \n" +
                                           "    case when e.CodEncuesta is null then '' else e.CodEncuesta end || '·'  ||\n" +
                                           "    case when e.Dispositivo is null then '' else e.Dispositivo end || '·' ||   \n" +
                                           "    case when e.Usuario is null then '' else e.Usuario end || '·' ||\n" +
                                           "    case when e.FechaInicial is null then '' else  cast(e.FechaInicial as varchar(100)) end || '·' ||\n" +
                                           "    case when e.FechaFinal is null then '' else  cast(e.FechaFinal as varchar(100)) end || '·' || \n" +
                                           "    case when e.estado is null then '' else e.estado end || '·' || \n" +
                                           "    case when e.NombreEncuesta is null then '' else  e.NombreEncuesta end || '·' || \n" +
                                           "    case when e.Tiempo is null then '' else e.Tiempo end || '·' || \n" +
                                           "    case when f.CodFormulario is null then '' else f.CodFormulario end || '·' || \n" +
                                           "    case when e.CodVisitaOrigen is null then '' else e.CodVisitaOrigen end,'^'),'') as resultado \n" +
                                           "FROM encuestas AS e \n" +
                                           "INNER JOIN versiones AS v ON v.codversion=e.codversion \n" +
                                           "INNER JOIN formularios AS f ON f.codformulario=v.codformulario \n" +
                                           "WHERE e.Sincronizado=0 AND v.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        header = reader.GetString(0);
                        Console.WriteLine(header);
                    }
                }

                transaction.Commit();
            }
            return header;
        }

        /// <summary>
        /// IMPORTANTE: Este script SQLite si difiere al de DCLib pero esta correctamente implementado.
        /// En DCLib le concatenan la , a los CodPreguntaSeccion por programación en Java mientras que con este script se hace utilizando GROUP_CONCAT. (getPreguntasLocalizacion)
        /// </summary>
        /// <returns></returns>
        public string GetLocalizationQuestions()
        {
            string localizationQuestions = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = "SELECT IFNULL(GROUP_CONCAT(''' || co.CodPreguntaSeccion || '''), '') FROM ColumnasObjeto AS co WHERE co.CodObjeto='COD_LOCALIZACION';";
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        localizationQuestions = reader.GetString(0);
                        if (!localizationQuestions.Equals(""))
                        {
                            localizationQuestions = " AND r.CodPreguntaSeccion NOT IN (" + localizationQuestions + ")";
                        }
                        Console.WriteLine(localizationQuestions);
                    }
                }

                transaction.Commit();
            }
            return localizationQuestions;
        }

        /// <summary>
        /// Respuestas (getDetalles)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetAnswer(string versionCode, string surveyCode)
        {
            string answer = "";
            string localizationQuestions = GetLocalizationQuestions();
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(\n" +
                                           "    case when r.CodPreguntaSeccion is null then '' else r.CodPreguntaSeccion end || '·' ||\n" +
                                           "    case when r.RespuestaAlfanumerico is null then '' else r.RespuestaAlfanumerico end || '·' ||\n" +
                                           "    case when r.CodOpcion is null then '' else r.CodOpcion end || '·' ||\n" +
                                           "    case when r.CodEntidad is null then '' else r.CodEntidad end || '·' ||\n" +
                                           "    case when r.CodObjeto is null then '' else r.CodObjeto end || '·' ||\n" +
                                           "    case when r.Agrupamiento is null then '' else r.Agrupamiento end || '·' ||\n" +
                                           "    case when r.FechaRegistro is null then '' else r.FechaRegistro end || '·' ||\n" +
                                           "    case when r.CodTipoDato is null then '' else r.CodTipoDato end , '^'),'') as resultado\n" +
                                           "FROM respuestas As r \n" +
                                           "INNER JOIN encuestas AS e ON r.codencuesta=e.codencuesta \n" +
                                           "WHERE e.Sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}'{2}", versionCode, surveyCode, localizationQuestions);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        answer = reader.GetString(0);
                        Console.WriteLine(answer);
                    }
                }

                transaction.Commit();
            }
            return answer;
        }

        /// <summary>
        /// Personas (getPersonas)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetPersons(string versionCode, string surveyCode)
        {
            string persons = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when p.codpersona is null then '' else p.codpersona end || '·' ||\n" +
                                                           "    case when p.apellidos is null then '' else p.apellidos end || '·' ||\n" +
                                                           "    case when p.nombres is null then '' else p.nombres end || '·' ||\n" +
                                                           "    case when p.fechanacimiento is null then '' else p.fechanacimiento end || '·' ||\n" +
                                                           "    case when p.codsexo is null then '' else p.codsexo end || '·' ||\n" +
                                                           "    case when p.codtipodocumento is null then '' else p.codtipodocumento end || '·' ||\n" +
                                                           "    case when p.nrodocumento is null then '' else p.nrodocumento end || '·' ||\n" +
                                                           "    case when p.nrocuit is null then '' else p.nrocuit end || '·' ||\n" +
                                                           "    case when p.lugarnacimiento is null then '' else p.lugarnacimiento end || '·' ||\n" +
                                                           "    case when p.codpaisorigen is null then '' else p.codpaisorigen end || '·' ||\n" +
                                                           "    case when p.codetnia is null then '' else p.codetnia end || '·' ||\n" +
                                                           "    case when p.codreligion is null then '' else p.codreligion end || '·' ||\n" +
                                                           "    case when p.codnacionalidad is null then '' else p.codnacionalidad end || '·' ||\n" +
                                                           "    case when p.perteneceetnia is null then '' else p.perteneceetnia end || '·' ||\n" +
                                                           "    case when p.CodTipoEstadoCivil is null then '' else p.CodTipoEstadoCivil end || '·' ||\n" +
                                                           "    case when p.FechaAlta is null then e.FechaInicial else p.FechaAlta end || '·' ||\n" +
                                                           "    case when p.CodTipoDocumentacion is null then '' else p.CodTipoDocumentacion end || '·' ||\n" +
                                                           "    case when p.OtraDocumentacion is null then '' else p.OtraDocumentacion end ,'^'),'') as resultado \n" +
                                                           "FROM personas as p inner join encuestas as e on e.codencuesta=p.codvisita \n" +
                                                           "WHERE e.sincronizado=0 and e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        persons = reader.GetString(0);
                        Console.WriteLine(persons);
                    }
                }

                transaction.Commit();
            }
            return persons;
        }

        /// <summary>
        /// Viviendas (getViviendas)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetHouseholds(string versionCode, string surveyCode)
        {
            string households = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat( " +
                                                           "    case when v.codvivienda is null then '' else v.codvivienda end || '·' ||\n" +
                                                           "    case when v.paiscod is null then '' else v.paiscod end || '·' ||\n" +
                                                           "    case when v.provcod is null then '' else v.provcod end || '·' ||\n" +
                                                           "    case when v.departamentocod is null then '' else v.departamentocod end || '·' ||\n" +
                                                           "    case when v.municipiocod is null then '' else v.municipiocod end  || '·' ||\n" +
                                                           "    case when v.localidadcod is null then '' else v.localidadcod end  || '·' ||\n" +
                                                           "    case when v.parajecod is null then '' else v.parajecod end || '·' ||\n" +
                                                           "    case when v.zonacod is null then '' else v.zonacod end  || '·' ||\n" +
                                                           "    case when v.barriocod is null then '' else v.barriocod end  || '·' ||\n" +
                                                           "    case when v.manzananum is null then '' else v.manzananum end  || '·' ||\n" +
                                                           "    case when v.manzanaletra is null then '' else v.manzanaletra end  || '·' ||\n" +
                                                           "    case when v.parcelanum is null then '' else v.parcelanum end  || '·' ||\n" +
                                                           "    case when v.callenombre is null then '' else v.callenombre end  || '·' ||\n" +
                                                           "    case when v.puertacasanumero is null then '' else v.puertacasanumero end || '·' ||\n" +
                                                           "    case when v.puertacasanumerointerior is null then '' else v.puertacasanumerointerior end || '·' ||\n" +
                                                           "    case when v.torre is null then '' else v.torre end || '·' ||\n" +
                                                           "    case when v.block is null then '' else v.block end || '·' ||\n" +
                                                           "    case when v.piso is null then '' else v.piso end || '·' ||\n" +
                                                           "    case when v.depto is null then '' else v.depto end || '·' ||\n" +
                                                           "    case when v.lote is null then '' else v.lote end || '·' ||\n" +
                                                           "    case when v.nim is null then '' else v.nim end || '·' ||\n" +
                                                           "    case when v.parcelaint is null then '' else v.parcelaint end || '·' ||\n" +
                                                           "    case when v.gps_x is null or v.gps_x=''  then '0' else cast(v.gps_x as varchar(100) ) end || '·' ||\n" +
                                                           "    case when v.gps_y is null or v.gps_y=''  then '0' else cast(v.gps_y as varchar(100) ) end || '·' ||\n" +
                                                           "    '·'|| --gpsaltura\n" +
                                                           "    '·'|| --gpsrangodistancia\n" +
                                                           "    case when v.barrioobs is null then '' else v.barrioobs end || '·' ||\n" +
                                                           "    case when v.calleobs is null then '' else v.calleobs end || '·' ||\n" +
                                                           "    case when v.zonaobs is null then '' else v.zonaobs end || '·' ||\n" +
                                                           "    case when v.referencias is null then '' else v.referencias end,'^'),'') as resultado \n" +
                                                           "FROM viviendas AS v \n" +
                                                           "INNER JOIN encuestas AS e ON e.codencuesta=v.codencuesta \n" +
                                                           "WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        households = reader.GetString(0);
                        Console.WriteLine(households);
                    }
                }

                transaction.Commit();
            }
            return households;
        }

        /// <summary>
        /// Localizaciones (getLocalizaciones)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetLocations(string versionCode, string surveyCode)
        {
            string locations = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat( " +
                                                           "    case when l.codlocalizacion is null then '' else l.codlocalizacion end || '·' ||\n" +
                                                           "    case when l.paiscod is null then '' else l.paiscod end || '·' ||\n" +
                                                           "    case when l.provcod is null then '' else l.provcod end || '·' ||\n" +
                                                           "    case when l.departamentocod is null then '' else l.departamentocod end || '·' ||\n" +
                                                           "    case when l.municipiocod is null then '' else l.municipiocod end  || '·' ||\n" +
                                                           "    case when l.localidadcod is null then '' else l.localidadcod end  || '·' ||\n" +
                                                           "    case when l.parajecod is null then '' else l.parajecod end || '·' ||\n" +
                                                           "    case when l.zonacod is null then '' else l.zonacod end  || '·' ||\n" +
                                                           "    case when l.barriocod is null then '' else l.barriocod end  || '·' || \n" +
                                                           "    case when l.gps_x is null or l.gps_x=''  then '0' else cast(l.gps_x as varchar(100) ) end || '·' ||\n" +
                                                           "    case when l.gps_y is null or l.gps_y='' then '0' else cast(l.gps_y as varchar(100) ) end || '·' ||\n" +
                                                           "    case when l.otroparaje is null or l.otroparaje='' then '' else l.otroparaje end || '·' ||\n" +
                                                           "    case when e.Distancia is null or e.Distancia='' then '0' else cast(e.Distancia as varchar(100) ) end , '^' ),'') as resultado \n" +
                                                           "FROM localizacion AS l \n" +
                                                           "INNER JOIN encuestas AS e on e.codencuesta=l.codvisita\n" +
                                                           "WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        locations = reader.GetString(0);
                        Console.WriteLine(locations);
                    }
                }

                transaction.Commit();
            }
            return locations;
        }

        /// <summary>
        /// Familias (getFamilias)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetFamilies(string versionCode, string surveyCode)
        {
            string families = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when f.codfamilia is null then '' else f.codfamilia end || '·' ||\n" +
                                                           "    case when f.apellido is null then '' else f.apellido end ,'^'),'') as resultado \n" +
                                                           "FROM familias AS f \n" +
                                                           "INNER JOIN encuestas AS e ON e.codencuesta=f.codvisita \n" +
                                                           "WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        families = reader.GetString(0);
                        Console.WriteLine(families);
                    }
                }

                transaction.Commit();
            }
            return families;
        }

        /// <summary>
        /// Familias Visitas (getFamiliasVisitas)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetFamiliesVisits(string versionCode, string surveyCode)
        {
            string familiesVisits = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when fv.codfamiliavisita is null then '' else fv.codfamiliavisita end || '·' ||\n" +
                                                           "    case when fv.codfamilia is null then '' else fv.codfamilia end || '·' ||\n" +
                                                           "    case when fv.codvivienda is null then '' else fv.codvivienda end,'^'),'') as resultado\n" +
                                                           "FROM encuestas AS e " +
                                                           "INNER JOIN viviendas AS v on v.codvivienda=fv.codvivienda AND v.codencuesta = '{1}'\n" +
                                                           "INNER JOIN Familias_Visitas AS fv ON fv.CodVisita=e.codencuesta \n" +
                                                           "WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        familiesVisits = reader.GetString(0);
                        Console.WriteLine(familiesVisits);
                    }
                }

                transaction.Commit();
            }
            return familiesVisits;
        }

        /// <summary>
        /// Personas Familias (getPersonasFamilias)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetPersonsFamilies(string versionCode, string surveyCode)
        {
            string personsfamilies = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when pf.codpersonafamilia is null then '' else pf.codpersonafamilia end || '·' ||\n" +
                                                           "    case when fv.codfamiliavisita is null then '' else fv.codfamiliavisita end || '·' ||\n" +
                                                           "    case when pf.codpersona is null then '' else pf.codpersona end || '·' ||\n" +
                                                           "    case when pf.Hogar is null then '' else pf.Hogar end || '·' ||\n" +
                                                           "    case when pf.JefeDeHogar is null then '' else pf.JefeDeHogar end ,'^'),'') as resultado \n" +
                                                           "FROM encuestas AS e \n" +
                                                           "INNER JOIN familias_visitas AS fv ON fv.codvisita=e.codencuesta \n" +
                                                           "INNER JOIN familias AS f ON f.codfamilia=fv.codfamilia \n" +
                                                           "INNER JOIN personas_familias AS pf ON pf.codfamiliavisita=fv.codfamiliavisita \n" +
                                                           "WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        personsfamilies = reader.GetString(0);
                        Console.WriteLine(personsfamilies);
                    }
                }

                transaction.Commit();
            }
            return personsfamilies;
        }

        /// <summary>
        /// Marcadores (getMarcadores)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetMarkers(string versionCode, string surveyCode)
        {
            string markers = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when m.CodMarcador is null then '' else m.CodMarcador end || '·' ||\n" +
                                                           "    case when m.CodFile is null then '' else m.CodFile end || '·' ||\n" +
                                                           "    case when m.CodSeccion is null then '' else m.CodSeccion end || '·' ||\n" +
                                                           "    case when m.Tiempo is null then '' else m.Tiempo end || '·' ||\n" +
                                                           "    case when m.Objeto is null then '' else m.Objeto end || '·' ||\n" +
                                                           "    case when m.Referencia is null then '' else m.Referencia end || '·' ||\n" +
                                                           "    case when m.Comentario is null then '' else m.Comentario end || '·' ||\n" +
                                                           "    case when m.Usuario is null then '' else m.Usuario end || '·' ||\n" +
                                                           "    case when m.Fecha is null then '''''' else cast(m.Fecha as varchar(100)) end , '^'),'') as resultado\n" +
                                                           "FROM marcadores AS m \n" +
                                                           "INNER JOIN Multimedia AS mu ON mu.CodMultimedia = m.CodFile \n" +
                                                           "INNER JOIN encuestas AS e ON e.CodEncuesta = mu.CodVisita \n" +
                                                           "WHERE m.Sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        markers = reader.GetString(0);
                        Console.WriteLine(markers);
                    }
                }

                transaction.Commit();
            }
            return markers;
        }

        /// <summary>
        /// Multimedia (getMultimedia)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetMultimedia(string versionCode, string surveyCode)
        {
            string multimedia = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(\n" +
                                                           "   case when m.CodMultimedia is null then '' else m.CodMultimedia end || '·' ||\n" +
                                                           "   case when m.CodObjeto is null then '' else m.CodObjeto end || '·' ||\n" +
                                                           "   case when m.CodEntidad is null then '' else m.CodEntidad end || '·' ||\n" +
                                                           "   case when m.CodSistema is null then '' else m.CodSistema end || '·' ||\n" +
                                                           "   case when m.CodTipoMultimedia is null then '' else m.CodTipoMultimedia end || '·' ||\n" +
                                                           "   case when m.FechaRegistro is null then '''''' else cast(m.FechaRegistro as varchar(100)) end || '·' ||\n" +
                                                           "   case when m.NameFile is null then '' else m.NameFile end || '·' ||\n" +
                                                           "   case when m.Duracion is null then '' else m.Duracion end  , '^'\n" +
                                                           "),'') as resultado \n" +
                                                           "FROM multimedia AS m \n" +
                                                           "INNER JOIN encuestas AS e ON e.CodEncuesta = m.CodVisita \n" +
                                                           "WHERE e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        multimedia = reader.GetString(0);
                        Console.WriteLine(multimedia);
                    }
                }

                transaction.Commit();
            }
            return multimedia;
        }

        /// <summary>
        /// Observaciones Multimedia (getObservacionesMultimedia)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetMultimediaObservation(string versionCode, string surveyCode)
        {
            string multimediaObservation = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(GROUP_CONCAT(\n" +
                                                           "    case when mo.CodMultimedia is null then '' else mo.CodMultimedia end || '·' ||\n" +
                                                           "    case when mo.CodTipoObservacion is null then '' else mo.CodTipoObservacion end || '·' ||\n" +
                                                           "    case when mo.FechaRegistro is null then '' else mo.FechaRegistro end , '^'),'') as resultado \n" +
                                                           "FROM Multimedia_TiposObservacion as mo\n" +
                                                           "INNER JOIN Multimedia as m on m.CodMultimedia = mo.CodMultimedia\n" +
                                                           "INNER JOIN encuestas as e on e.CodEncuesta = m.CodVisita\n" +
                                                           "WHERE e.CodVersion = '{0}' and e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        multimediaObservation = reader.GetString(0);
                        Console.WriteLine(multimediaObservation);
                    }
                }

                transaction.Commit();
            }
            return multimediaObservation;
        }

        /// <summary>
        /// Relación Parentezco (getRelacionParentesco)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetRelationship(string versionCode, string surveyCode)
        {
            string relationship = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when rp.CodRelacionParentesco is null then '' else rp.CodRelacionParentesco end || '·' ||\n" +
                                                           "    case when rp.CodPersona is null then '' else rp.CodPersona end || '·' ||\n" +
                                                           "    case when rp.CodPersonaRelacion is null then '' else rp.CodPersonaRelacion end || '·' ||\n" +
                                                           "    case when rp.CodTipoParentesco is null then '' else rp.CodTipoParentesco end || '·' ||\n" +
                                                           "    case when rp.FechaRegistro is null then '' else rp.FechaRegistro end || '·' ||\n" +
                                                           "    case when rp.Activo is null then '' else rp.Activo end || '·' ||\n" +
                                                           "    case when rp.CodTipoParentescoInverso is null then '' else rp.CodTipoParentescoInverso end ,'^'),'') as resultado \n" +
                                                           "FROM relacion_parentesco AS rp \n" +
                                                           "INNER JOIN encuestas AS e ON e.codencuesta=rp.CodVisita \n" +
                                                           "WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        relationship = reader.GetString(0);
                        Console.WriteLine(relationship);
                    }
                }

                transaction.Commit();
            }
            return relationship;
        }

        /// <summary>
        /// Errores (getErrores)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <param name="surveyCode"></param>
        /// <returns></returns>
        public string GetError(string versionCode, string surveyCode)
        {
            string error = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT ifnull(group_concat(" +
                                                           "    case when err.CodEntidad is null then '' else err.CodEntidad end || '·' ||\n" +
                                                           "    case when err.CodPreguntaSeccion is null then '' else err.CodPreguntaSeccion end || '·' ||\n" +
                                                           "    case when err.Pregunta is null then '' else err.Pregunta end || '·' ||\n" +
                                                           "    case when err.Entidad is null then '' else err.Entidad end || '·' ||\n" +
                                                           "    case when err.CodSeccionModulo is null then '' else err.CodSeccionModulo end || '·' ||\n" +
                                                           "    case when err.SeccionModulo is null then '' else err.SeccionModulo end || '·' ||\n" +
                                                           "    case when err.CodModulo is null then '' else err.CodModulo end || '·' ||\n" +
                                                           "    case when err.Visitada is null then 0 else err.Visitada end || '·' ||\n" +
                                                           "    case when err.OrdenSeccionModulo is null then 0 else err.OrdenSeccionModulo end || '·' ||\n" +
                                                           "    case when err.OrdenPreguntaSeccion is null then 0 else err.OrdenPreguntaSeccion end || '·' ||\n" +
                                                           "    case when err.Agrupamiento is null then '' else err.Agrupamiento end || '·' ||\n" +
                                                           "    case when err.Activo is null then 0 else err.Activo end ,'^'),'') as resultado \n" +
                                                           " FROM Errores AS err \n" +
                                                           " INNER JOIN encuestas AS e on e.codencuesta=err.CodVisita \n" +
                                                           " WHERE e.sincronizado=0 AND e.CodVersion = '{0}' AND e.codencuesta='{1}';", versionCode, surveyCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        error = reader.GetString(0);
                        Console.WriteLine(error);
                    }
                }

                transaction.Commit();
            }
            return error;
        }

        /// <summary>
        /// Log Sincronización Encuestas (getLogSincronizacionEncuestas)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <returns></returns>
        public string GetSurveySynchronizationLog(string versionCode)
        {
            string surveySynchronizationLog = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT group_concat(ifnull(e.CodEncuesta,'')||','||strftime('%d/%m/%Y', ifnull(e.FechaInicial,''))||','||ifnull(e.Usuario,'')||','||ifnull(e.Estado,''),'|') AS result " +
                                                           "FROM encuestas as e " +
                                                           "WHERE e.Sincronizado = 0 AND e.CodVersion = '{0}';", versionCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            surveySynchronizationLog = reader.GetString(0);
                            Console.WriteLine(surveySynchronizationLog);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                transaction.Commit();
            }
            return surveySynchronizationLog;
        }

        /// <summary>
        /// Log Sincronización Multimedia (getLogSincronizacionMultimedia)
        /// </summary>
        /// <param name="versionCode"></param>
        /// <returns></returns>
        public string GetMultimediaSynchronizationLog(string versionCode)
        {
            string multimediaSynchronizationLog = "";
            using (var transaction = _sqliteConnection.BeginTransaction())
            {
                var selectCommand = _sqliteConnection.CreateCommand();
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = string.Format("SELECT group_concat(ifnull(m.CodMultimedia,'')||','||ifnull(m.NameFile,'')||','||ifnull(m.CodVisita,'')||','||ifnull(m.CodTipoMultimedia,''),'|') AS result \n" +
                                                           "FROM Multimedia AS m \n" +
                                                           "INNER JOIN encuestas AS e ON m.CodVisita = e.CodEncuesta \n" +
                                                           "LEFT JOIN Multimedia_TiposObservacion AS mto ON mto.CodMultimedia = m.CodMultimedia \n" +
                                                           "WHERE e.CodVersion = '{0}' AND e.Estado <> '' \n" +
                                                           "AND m.Sincronizado=0 \n" +
                                                           "AND m.CodClasificacionArchivo = '';", versionCode);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            multimediaSynchronizationLog = reader.GetString(0);
                            Console.WriteLine(multimediaSynchronizationLog);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                transaction.Commit();
            }
            return multimediaSynchronizationLog;
        }

        public int UpdateIsSynchronizedSurvey(string surveyCode, bool isSynchronized)
        {
            var command = _sqliteConnection.CreateCommand();
            command.CommandText = string.Format("UPDATE encuestas SET sincronizado = {0} WHERE CodEncuesta = '{1}';", isSynchronized ? 1 : 0, surveyCode);

            //command.CommandText = "UPDATE encuestas SET sincronizado = $isSynchronized WHERE CodEncuesta = '$surveyCode';";
            //command.Parameters.AddWithValue("$isSynchronized", isSynchronized ? 1 : 0);
            //command.Parameters.AddWithValue("$surveyCode", surveyCode);

            return command.ExecuteNonQuery();
        }

        public int UpdateIsSynchronizedMultimedia(string multimediaCode, bool isSynchronized)
        {
            var command = _sqliteConnection.CreateCommand();
            command.CommandText = string.Format("UPDATE Multimedia SET Sincronizado = {0} WHERE CodMultimedia = '{1}';", isSynchronized ? 1 : 0, multimediaCode);

            //command.CommandText = "UPDATE encuestas SET sincronizado = $isSynchronized WHERE CodEncuesta = '$surveyCode';";
            //command.Parameters.AddWithValue("$isSynchronized", isSynchronized ? 1 : 0);
            //command.Parameters.AddWithValue("$surveyCode", surveyCode);

            return command.ExecuteNonQuery();
        }
    }
}