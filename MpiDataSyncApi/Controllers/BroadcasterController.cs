﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using MpiDataSyncApi.Helpers;
using MpiDataSyncApi.Hubs;
using MpiDataSyncApi.Models.Devices;
using MpiDataSyncApi.Models.Extractions;
using MpiDataSyncApi.Models.Files;
using MpiDataSyncApi.Models.Settings;
using MpiDataSyncApi.Models.Surveys;
using MpiDataSyncApi.Models.Surveys.Dto;
using MpiDataSyncApi.Models.Synchronizations;
using MpiDataSyncApi.Models.Synchronizations.Dto;
using MpiDataSyncApi.Services;
using Newtonsoft.Json;
using File = MpiDataSyncApi.Models.Files.File;


namespace MpiDataSyncApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BroadcasterController : Controller
    {
        IHubContext<Broadcaster, IBroadcaster> _hubContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IExtractionRepository _extractionRepository;
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISettingRepository _settingRepository;
        private readonly string _extractedFilesBaseDirectory;
        private readonly ILogger<BroadcasterController> _logger;

        public BroadcasterController(
            IHostingEnvironment hostingEnvironment,
            IDeviceRepository deviceRepository,
            IFileRepository fileRepository,
            IExtractionRepository extractionRepository,
            ISurveyRepository surveyRepository,
            ISettingRepository settingRepository,
            IHubContext<Broadcaster, IBroadcaster> hubContext,
            ILogger<BroadcasterController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            _deviceRepository = deviceRepository;
            _fileRepository = fileRepository;
            _extractionRepository = extractionRepository;
            _surveyRepository = surveyRepository;
            _settingRepository = settingRepository;
            _extractedFilesBaseDirectory = _hostingEnvironment.ContentRootPath + @"\ExtractedFiles";
            _logger = logger;
            _hubContext = hubContext;
        }

        [HttpGet, Route("ping")]
        public async Task<IActionResult> Ping()
        {
            return Ok(new
            {
                FilesPath = _hostingEnvironment.ContentRootPath
            });
        }

        [HttpPost, Route("upload-multiple-files")]
        public async Task<IActionResult> UploadMultipleFiles(IFormCollection formCollection)
        {
            _logger.LogInformation("Extracción > Iniciando transferencia de archivos...");
            string extractionCode = Guid.NewGuid().ToString();
            try
            {
                string deviceJson = "";

                foreach (var key in formCollection.Keys)
                {
                    foreach (var value in formCollection[key])
                    {
                        switch (key)
                        {
                            case "device":
                                deviceJson = value;
                                break;
                            default:
                                break;
                        }
                    }
                }

                if (deviceJson == null || deviceJson.Equals(""))
                {
                    return BadRequest(new
                    {
                        ResultCode = 126,
                        ResultMsg = "Faltan parámetros."
                    });
                }

                _logger.LogInformation("Extracción > Parámetros correctos: " + deviceJson);

                Device device = JsonConvert.DeserializeObject<Device>(deviceJson);
                Device deviceCopy = device;

                //extractionCode = device.Extractions.FirstOrDefault().ExtractionCode;

                string extractionDirectory = _extractedFilesBaseDirectory + $@"\{device.DeviceCode}\{extractionCode}";
                Directory.CreateDirectory(extractionDirectory);

                List<Extraction> extractiosnEnabled = new List<Extraction>();
                foreach (var extraction in device.Extractions)
                {
                    if (extraction.IsExtractionEnabled)
                        extractiosnEnabled.Add(extraction);
                }

                device.Extractions.Clear();
                foreach (var extraction in extractiosnEnabled)
                {
                    device.Extractions.Add(extraction);
                }

                foreach (var extraction in device.Extractions)
                {
                    extraction.ExtractionCode = extractionCode;
                    foreach (var survey in extraction.Surveys)
                    {
                        _surveyRepository.RemoveByExtractionCodeAndSurveyCode(survey.SurveyCode);   //extraction.ExtractionCode
                        _surveyRepository.Save();
                    }

                    foreach (var file in extraction.Files)
                    {
                        _fileRepository.RemoveByExtractionCodeAndSurveyCode(file.SurveyCode);    //extraction.ExtractionCode
                        _fileRepository.Save();
                    }
                }

                Device d = _deviceRepository.GetDeviceByDeviceCode(device.DeviceCode, false);
                if (d == null)
                {
                    // Insert
                    _logger.LogInformation("Extracción > El dispositivo no existia previamente, entonces lo inserto.");
                    _deviceRepository.Add(device);
                }
                else
                {
                    // Update
                    _logger.LogInformation("Extracción > El dispositivo ya existia, entonces lo actualizo agregandole una nueva extracción.");
                    d.Extractions = new List<Extraction>();
                    foreach (var extraction in device.Extractions)
                    {
                        d.Extractions.Add(extraction);
                    }
                    _deviceRepository.Edit(d);
                }
                _deviceRepository.Save();

                _logger.LogInformation("Extracción > Iniciando copia de archivos en carpeta destino: " + extractionDirectory);
                long size = 0;
                foreach (var file in formCollection.Files)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
                    var filePath = extractionDirectory + $@"\{filename}";
                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    File f = _fileRepository.GetFileByFilename(filename);
                    if (f != null)
                    {
                        f.IsExtracted = true;
                        _fileRepository.Edit(f);
                        _fileRepository.Save();
                    }
                }
                _logger.LogInformation("Extracción > Finalizo la copia de los archivos en la carpeta destino: " + extractionDirectory);
                _logger.LogInformation("Extracción > Cantidad de Kb copiados: " + Math.Round((decimal)size / 1024, 2));

                //deviceCopy.Status.IsReadyToSync = true;
                //Clients.Group(Broadcaster.DevicesGroupId).StatusChange(deviceCopy);

                return Ok(new
                {
                    ResultCode = 1,
                    ResultMsg = "Archivos subidos correctamente.",
                    TotalSizeInBytes = size,
                    TotalSizeInKb = Math.Round((decimal)size / 1024, 2)
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(125, ex, "Extracción > Ocurrio un error en el proceso de extracción " + ex.Message);
                long extractionId = _extractionRepository.GetIdByCode(extractionCode);
                if (extractionId > 0)
                {
                    _logger.LogInformation("Extracción > Revirtiendo cambios.");
                    _surveyRepository.RemoveByExtractionId(extractionId);
                    _surveyRepository.Save();
                    _fileRepository.RemoveByExtractionId(extractionId);
                    _fileRepository.Save();
                    _extractionRepository.RemoveById(extractionId);
                    _extractionRepository.Save();
                    _logger.LogInformation("Extracción > Finalizo el proceso de revertir cambios.");
                }

                return BadRequest(new
                {
                    ResultCode = 125,
                    ResultMsg = "" + ex.Message
                });
            }
        }

        [HttpGet, Route("devices-info")]
        public async Task<IActionResult> GetDevicesInfo()
        {
            try
            {
                _logger.LogInformation("GetDevicesInfo > Iniciando...");
                List<Device> devices = _deviceRepository.GetDevices();
                //_logger.LogInformation("GetDevicesInfo > devices: " + JsonConvert.SerializeObject(devices, Formatting.Indented, new JsonSerializerSettings()
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //}));

                //_deviceRepository.GetDevicesInfo();

                foreach (var device in devices)
                {
                    foreach (var extraction in device.Extractions)
                    {
                        var synchronization = device.Synchronizations.FirstOrDefault(s => s.FormCode == extraction.FormCode);
                        if (synchronization == null)
                        {
                            Synchronization s = new Synchronization
                            {
                                SynchronizationCode = extraction.ExtractionCode,
                                FormCode = extraction.FormCode,
                                FormName = extraction.FormName,
                                VersionCode = extraction.VersionCode,
                                SurveysQuantity = 0,
                                NewsQuantity = 0,
                                InterestPointsQuantity = 0,
                                MultimediaQuantity = 0,
                                UploadedSurveysQuantity = 0,
                                UploadedNewsQuantity = 0,
                                UploadedInterestPointsQuantity = 0,
                                UploadedMultimediaQuantity = 0
                            };
                            foreach (var file in extraction.Files)
                            {
                                if (!file.IsSynchronized)
                                {
                                    File fileCopy = file;
                                    fileCopy.Extraction = null;
                                    s.Files.Add(fileCopy);
                                    s.MultimediaQuantity++;
                                }
                            }
                            foreach (var survey in extraction.Surveys)
                            {
                                if (!survey.IsSynchronized)
                                {
                                    s.Surveys.Add(survey);
                                    switch (survey.StatusCode)
                                    {
                                        case "COD_COMPLETA":
                                        case "COD_FINALIZADA_INCOMPLETA":
                                        case "COD_EN_PROCESO":
                                        case "COD_DE_PRUEBA":
                                            s.SurveysQuantity++;
                                            break;
                                        case "COD_IMPOSIBILITADA":
                                            s.NewsQuantity++;
                                            break;
                                        case "COD_PUNTO_INTERES":
                                            s.InterestPointsQuantity++;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            device.Synchronizations.Add(s);
                        }
                        else
                        {
                            //synchronization.SurveysQuantity += extraction.SurveysQuantity;
                            //synchronization.NewsQuantity += extraction.NewsQuantity;
                            //synchronization.InterestPointsQuantity += extraction.InterestPointsQuantity;
                            //synchronization.MultimediaQuantity += extraction.MultimediaQuantity;
                            foreach (var file in extraction.Files)
                            {
                                if (!file.IsSynchronized)
                                {
                                    File fileCopy = file;
                                    fileCopy.Extraction = null;
                                    synchronization.Files.Add(fileCopy);
                                    synchronization.MultimediaQuantity++;
                                }
                            }
                            foreach (var survey in extraction.Surveys)
                            {
                                if (!survey.IsSynchronized)
                                {
                                    synchronization.Surveys.Add(survey);
                                    switch (survey.StatusCode)
                                    {
                                        case "COD_COMPLETA":
                                        case "COD_FINALIZADA_INCOMPLETA":
                                        case "COD_EN_PROCESO":
                                        case "COD_DE_PRUEBA":
                                            synchronization.SurveysQuantity++;
                                            break;
                                        case "COD_IMPOSIBILITADA":
                                            synchronization.NewsQuantity++;
                                            break;
                                        case "COD_PUNTO_INTERES":
                                            synchronization.InterestPointsQuantity++;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    device.Status = new Status
                    {
                        IsReadyToExtraction = false,
                        IsReadyToSync = device.Synchronizations.Any(s => s.Files.Count > 0 || s.Surveys.Count > 0)
                    };

                    device.Progress = new Progress
                    {
                        MinimumInBytes = 0,
                        MaximumInBytes = 0,
                        ValueInBytes = 0,
                        ValueInPercentage = 0,
                        IsExtracting = false,
                        IsExtractionComplete = false,
                        IsExtractionSuccess = false,
                        IsSynchronizing = false,
                        IsSynchronizationComplete = false,
                        IsSynchronizationSuccess = false
                    };
                }

                //_logger.LogInformation("GetDevicesInfo > OK > devices: " + JsonConvert.SerializeObject(devices, Formatting.Indented, new JsonSerializerSettings()
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //}));
                return Ok(new
                {
                    Devices = devices
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(125, ex, "GetDevicesInfo > Error: " + ex.Message);
                return BadRequest(new
                {
                    ResultCode = 125,
                    ResultMsg = "" + ex.Message
                });
            }
        }

        /// <summary>
        /// Obtiene la cantidad de dispositivos
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("devices/quantity")]
        public async Task<IActionResult> GetDevicesQuantity()
        {
            var result = _deviceRepository.GetDevicesQuantity();
            return Ok(new
            {
                ResultCode = 1,
                ResultMsg = "Solicitud procesada correctamente.",
                Result = result
            });
        }

        /// <summary>
        /// Obtiene una lista de dispositivos utilizando paginación.
        /// </summary>
        /// <param name="currentPageNumber">Número de página actual</param>
        /// <param name="numberOfRecordsPerPage">Cantidad de registros por página</param>
        /// <returns></returns>
        [HttpGet, Route("v2/devices-info/{currentPageNumber}/{numberOfRecordsPerPage}")]
        public async Task<IActionResult> GetDevicesInfoV2(int currentPageNumber, int numberOfRecordsPerPage)
        {
            try
            {
                _logger.LogInformation("GetDevicesInfo > Iniciando...");
                List<Device> devices = _deviceRepository.GetDevices(currentPageNumber, numberOfRecordsPerPage);
                //_logger.LogInformation("GetDevicesInfo > devices: " + JsonConvert.SerializeObject(devices, Formatting.Indented, new JsonSerializerSettings()
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //}));

                //_deviceRepository.GetDevicesInfo();

                foreach (var device in devices)
                {
                    foreach (var extraction in device.Extractions)
                    {
                        var synchronization = device.Synchronizations.FirstOrDefault(s => s.FormCode == extraction.FormCode);
                        if (synchronization == null)
                        {
                            Synchronization s = new Synchronization
                            {
                                SynchronizationCode = extraction.ExtractionCode,
                                FormCode = extraction.FormCode,
                                FormName = extraction.FormName,
                                VersionCode = extraction.VersionCode,
                                SurveysQuantity = 0,
                                NewsQuantity = 0,
                                InterestPointsQuantity = 0,
                                MultimediaQuantity = 0,
                                UploadedSurveysQuantity = 0,
                                UploadedNewsQuantity = 0,
                                UploadedInterestPointsQuantity = 0,
                                UploadedMultimediaQuantity = 0
                            };
                            foreach (var file in extraction.Files)
                            {
                                if (!file.IsSynchronized)
                                {
                                    File fileCopy = file;
                                    fileCopy.Extraction = null;
                                    s.Files.Add(fileCopy);
                                    s.MultimediaQuantity++;
                                }
                            }
                            foreach (var survey in extraction.Surveys)
                            {
                                if (!survey.IsSynchronized)
                                {
                                    s.Surveys.Add(survey);
                                    switch (survey.StatusCode)
                                    {
                                        case "COD_COMPLETA":
                                        case "COD_FINALIZADA_INCOMPLETA":
                                        case "COD_EN_PROCESO":
                                        case "COD_DE_PRUEBA":
                                            s.SurveysQuantity++;
                                            break;
                                        case "COD_IMPOSIBILITADA":
                                            s.NewsQuantity++;
                                            break;
                                        case "COD_PUNTO_INTERES":
                                            s.InterestPointsQuantity++;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            device.Synchronizations.Add(s);
                        }
                        else
                        {
                            //synchronization.SurveysQuantity += extraction.SurveysQuantity;
                            //synchronization.NewsQuantity += extraction.NewsQuantity;
                            //synchronization.InterestPointsQuantity += extraction.InterestPointsQuantity;
                            //synchronization.MultimediaQuantity += extraction.MultimediaQuantity;
                            foreach (var file in extraction.Files)
                            {
                                if (!file.IsSynchronized)
                                {
                                    File fileCopy = file;
                                    fileCopy.Extraction = null;
                                    synchronization.Files.Add(fileCopy);
                                    synchronization.MultimediaQuantity++;
                                }
                            }
                            foreach (var survey in extraction.Surveys)
                            {
                                if (!survey.IsSynchronized)
                                {
                                    synchronization.Surveys.Add(survey);
                                    switch (survey.StatusCode)
                                    {
                                        case "COD_COMPLETA":
                                        case "COD_FINALIZADA_INCOMPLETA":
                                        case "COD_EN_PROCESO":
                                        case "COD_DE_PRUEBA":
                                            synchronization.SurveysQuantity++;
                                            break;
                                        case "COD_IMPOSIBILITADA":
                                            synchronization.NewsQuantity++;
                                            break;
                                        case "COD_PUNTO_INTERES":
                                            synchronization.InterestPointsQuantity++;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    device.Status = new Status
                    {
                        IsReadyToExtraction = false,
                        IsReadyToSync = device.Synchronizations.Any(s => s.Files.Count > 0 || s.Surveys.Count > 0)
                    };

                    device.Progress = new Progress
                    {
                        MinimumInBytes = 0,
                        MaximumInBytes = 0,
                        ValueInBytes = 0,
                        ValueInPercentage = 0,
                        IsExtracting = false,
                        IsExtractionComplete = false,
                        IsExtractionSuccess = false,
                        IsSynchronizing = false,
                        IsSynchronizationComplete = false,
                        IsSynchronizationSuccess = false
                    };
                }

                //_logger.LogInformation("GetDevicesInfo > OK > devices: " + JsonConvert.SerializeObject(devices, Formatting.Indented, new JsonSerializerSettings()
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //}));

                return Ok(new
                {
                    Devices = devices
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(125, ex, "GetDevicesInfo > Error: " + ex.Message);
                return BadRequest(new
                {
                    ResultCode = 125,
                    ResultMsg = "" + ex.Message
                });
            }
        }

        [HttpGet, Route("devices-info/{deviceCode}")]
        public async Task<IActionResult> GetDeviceInfoByDeviceCode(string deviceCode)
        {
            Device device = _deviceRepository.GetDeviceByDeviceCode(deviceCode, true);

            foreach (var extraction in device.Extractions)
            {
                var synchronization = device.Synchronizations.FirstOrDefault(s => s.FormCode == extraction.FormCode);
                if (synchronization == null)
                {
                    Synchronization s = new Synchronization
                    {
                        SynchronizationCode = extraction.ExtractionCode,
                        FormCode = extraction.FormCode,
                        FormName = extraction.FormName,
                        VersionCode = extraction.VersionCode,
                        SurveysQuantity = 0,
                        NewsQuantity = 0,
                        InterestPointsQuantity = 0,
                        MultimediaQuantity = 0,
                        UploadedSurveysQuantity = 0,
                        UploadedNewsQuantity = 0,
                        UploadedInterestPointsQuantity = 0,
                        UploadedMultimediaQuantity = 0
                    };
                    foreach (var file in extraction.Files)
                    {
                        if (!file.IsSynchronized)
                        {
                            File fileCopy = file;
                            fileCopy.Extraction = null;
                            s.Files.Add(fileCopy);
                            s.MultimediaQuantity++;
                        }
                    }
                    foreach (var survey in extraction.Surveys)
                    {
                        if (!survey.IsSynchronized)
                        {
                            s.Surveys.Add(survey);
                            switch (survey.StatusCode)
                            {
                                case "COD_COMPLETA":
                                case "COD_FINALIZADA_INCOMPLETA":
                                case "COD_EN_PROCESO":
                                case "COD_DE_PRUEBA":
                                    s.SurveysQuantity++;
                                    break;
                                case "COD_IMPOSIBILITADA":
                                    s.NewsQuantity++;
                                    break;
                                case "COD_PUNTO_INTERES":
                                    s.InterestPointsQuantity++;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    device.Synchronizations.Add(s);
                }
                else
                {
                    //synchronization.SurveysQuantity += extraction.SurveysQuantity;
                    //synchronization.NewsQuantity += extraction.NewsQuantity;
                    //synchronization.InterestPointsQuantity += extraction.InterestPointsQuantity;
                    //synchronization.MultimediaQuantity += extraction.MultimediaQuantity;
                    foreach (var file in extraction.Files)
                    {
                        if (!file.IsSynchronized)
                        {
                            File fileCopy = file;
                            fileCopy.Extraction = null;
                            synchronization.Files.Add(fileCopy);
                            synchronization.MultimediaQuantity++;
                        }
                    }
                    foreach (var survey in extraction.Surveys)
                    {
                        if (!survey.IsSynchronized)
                        {
                            synchronization.Surveys.Add(survey);
                            switch (survey.StatusCode)
                            {
                                case "COD_COMPLETA":
                                case "COD_FINALIZADA_INCOMPLETA":
                                case "COD_EN_PROCESO":
                                case "COD_DE_PRUEBA":
                                    synchronization.SurveysQuantity++;
                                    break;
                                case "COD_IMPOSIBILITADA":
                                    synchronization.NewsQuantity++;
                                    break;
                                case "COD_PUNTO_INTERES":
                                    synchronization.InterestPointsQuantity++;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            device.Status = new Status
            {
                IsReadyToExtraction = false,
                IsReadyToSync = device.Synchronizations.Any(s => s.Files.Count > 0 || s.Surveys.Count > 0)
            };

            device.Progress = new Progress
            {
                MinimumInBytes = 0,
                MaximumInBytes = 0,
                ValueInBytes = 0,
                ValueInPercentage = 0,
                IsExtracting = false,
                IsExtractionComplete = false,
                IsExtractionSuccess = false,
                IsSynchronizing = false,
                IsSynchronizationComplete = false,
                IsSynchronizationSuccess = false
            };

            string deviceStr = JsonConvert.SerializeObject(device, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            Device ignoredJsonLoopDevice = JsonConvert.DeserializeObject<Device>(deviceStr);
            _hubContext.Clients.Group(Broadcaster.DevicesGroupId).ExtractionComplete(ignoredJsonLoopDevice);

            return Ok(new
            {
                Device = device
            });
        }

        [HttpPost, Route("sync-surveys-and-files")]
        public async Task<IActionResult> SyncSurveysAndFiles([FromBody] Device device)
        {
            var result = true;
            SQLiteService SQLiteService = new SQLiteService();
            try
            {
                foreach (var synchronization in device.Synchronizations)
                {
                    LogDto logDto = new LogDto();
                    logDto.CodVersion = synchronization.VersionCode;
                    logDto.Modalidad = "Extractor";
                    logDto.Dispositivo = device.DeviceCode;
                    //string macAddress = await NetworkService.Instance.GetMacAddress();
                    //logDto.DispositivoExtraccion = macAddress ?? "Desconocido";
                    string hostName = await NetworkService.Instance.GetHostName();
                    logDto.DispositivoExtraccion = hostName ?? "Desconocido";

                    List<Extraction> extractions = _extractionRepository.GetExtractionsByDeviceAndVersionCode(device.DeviceCode, synchronization.VersionCode);

                    foreach (var extraction in extractions)
                    {
                        SQLiteService.OpenConnection(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode);
                        logDto.DetalleLog += SQLiteService.GetSurveySynchronizationLog(synchronization.VersionCode) + "|";
                        logDto.DetalleMultimediaLog += SQLiteService.GetMultimediaSynchronizationLog(synchronization.VersionCode) + "|";
                        SQLiteService.CloseConnection();
                    }

                    if (logDto.DetalleLog.EndsWith("|"))
                    {
                        logDto.DetalleLog = logDto.DetalleLog.Remove(logDto.DetalleLog.Length - 1);
                    }

                    if (logDto.DetalleMultimediaLog.EndsWith("|"))
                    {
                        logDto.DetalleMultimediaLog = logDto.DetalleMultimediaLog.Remove(logDto.DetalleMultimediaLog.Length - 1);
                    }

                    bool uploadSynchronizationLogResult = false;
                    if (!logDto.DetalleLog.Equals("") || !logDto.DetalleMultimediaLog.Equals(""))
                    {
                        uploadSynchronizationLogResult = await SynchronizationService.GetInstance(_settingRepository, device.Environment).UploadSynchronizationLog(logDto);
                    }

                    if (!uploadSynchronizationLogResult)
                    {
                        continue;
                    }

                    foreach (var extraction in extractions)
                    {
                        SQLiteService.OpenConnection(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode);
                        //List<Survey> surveys = _surveyRepository.GetNonSynchronizedSurveysByExtractionCode(extraction.ExtractionCode);
                        List<Survey> surveys = _surveyRepository.GetNonSynchronizedSurveysByExtractionId(extraction.Id);

                        foreach (var survey in surveys)
                        {
                            try
                            {
                                UploadSurveyDto uploadSurveyDto = new UploadSurveyDto
                                {
                                    SurveyCode = survey.SurveyCode,
                                    VersionCode = synchronization.VersionCode,
                                    ApplicationCode = SQLiteService.GetApplicationCode(synchronization.FormCode),
                                    Header = SQLiteService.GetHeader(synchronization.VersionCode, survey.SurveyCode),
                                    Answer = CompressionHelper.GzipCompress(SQLiteService.GetAnswer(synchronization.VersionCode, survey.SurveyCode)),
                                    Persons = SQLiteService.GetPersons(synchronization.VersionCode, survey.SurveyCode),
                                    Households = SQLiteService.GetHouseholds(synchronization.VersionCode, survey.SurveyCode),
                                    Location = SQLiteService.GetLocations(synchronization.VersionCode, survey.SurveyCode),
                                    Families = SQLiteService.GetFamilies(synchronization.VersionCode, survey.SurveyCode),
                                    FamiliesVisits = SQLiteService.GetFamiliesVisits(synchronization.VersionCode, survey.SurveyCode),
                                    PersonsFamilies = SQLiteService.GetPersonsFamilies(synchronization.VersionCode, survey.SurveyCode),
                                    Markers = CompressionHelper.GzipCompress(SQLiteService.GetMarkers(synchronization.VersionCode, survey.SurveyCode)),
                                    Multimedia = SQLiteService.GetMultimedia(synchronization.VersionCode, survey.SurveyCode),
                                    MultimediaObservation = SQLiteService.GetMultimediaObservation(synchronization.VersionCode, survey.SurveyCode),
                                    Relationship = CompressionHelper.GzipCompress(SQLiteService.GetRelationship(synchronization.VersionCode, survey.SurveyCode)),
                                    SurveyErrors = CompressionHelper.GzipCompress(SQLiteService.GetError(synchronization.VersionCode, survey.SurveyCode)),
                                    ExtractionCode = ""
                                };

                                bool uploadSurveyResult = await SynchronizationService.GetInstance(_settingRepository, device.Environment).UploadSurvey(uploadSurveyDto);
                                if (uploadSurveyResult)
                                {
                                    //survey.IsSynchronized = true;
                                    //_surveyRepository.Edit(survey);
                                    _surveyRepository.SetIsSynchronizedBySurveyCode(survey.SurveyCode, true);
                                    int editSurveyResult = _surveyRepository.Save();
                                    int setIsSynchronizedSurveyResult = SQLiteService.UpdateIsSynchronizedSurvey(survey.SurveyCode, true);
                                    if (editSurveyResult > 0 && setIsSynchronizedSurveyResult > 0)
                                    {
                                        switch (survey.StatusCode)
                                        {
                                            case "COD_COMPLETA":
                                            case "COD_FINALIZADA_INCOMPLETA":
                                            case "COD_EN_PROCESO":
                                            case "COD_DE_PRUEBA":
                                                synchronization.UploadedSurveysQuantity++;
                                                break;
                                            case "COD_IMPOSIBILITADA":
                                                synchronization.UploadedNewsQuantity++;
                                                break;
                                            case "COD_PUNTO_INTERES":
                                                synchronization.UploadedInterestPointsQuantity++;
                                                break;
                                            default:
                                                break;
                                        }
                                        synchronization.Surveys.First(s => s.SurveyCode == survey.SurveyCode).IsSynchronized = true;

                                        _hubContext.Clients.Group(Broadcaster.DevicesGroupId).StatusChange(device);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                result = false;
                                _logger.LogError(125, ex, $"Sincronización > Ocurrio un error sincronizando la encuesta \"{survey.SurveyCode}\" de la extracción \"{extraction.ExtractionCode}\" del dispositivo \"{device.DeviceCode}\". Detalle: " + ex.Message);
                            }
                        }
                        SQLiteService.CloseConnection();
                    }

                    //foreach (var extraction in extractions)
                    //{
                    //    SQLiteService.OpenConnection(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode);
                    //    List<File> files = _fileRepository.GetNonSynchronizedFilesByExtractionCode(extraction.ExtractionCode);
                    //    foreach (var file in files)
                    //    {
                    //        try
                    //        {
                    //            bool uploadFileResult = await SynchronizationService.GetInstance(_settingRepository).UploadFile(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode, extraction.FormCode, extraction.VersionCode, file);
                    //            if (uploadFileResult)
                    //            {
                    //                //file.IsSynchronized = true;
                    //                //_fileRepository.Edit(file);
                    //                _fileRepository.SetIsSynchronizedByMultimediaCode(file.MultimediaCode, true);
                    //                int editFileResult = _fileRepository.Save();
                    //                int setIsSynchronizedMultimediaResult = SQLiteService.UpdateIsSynchronizedMultimedia(file.MultimediaCode, true);
                    //                if (editFileResult > 0 && setIsSynchronizedMultimediaResult > 0)
                    //                {
                    //                    synchronization.UploadedMultimediaQuantity++;
                    //                    synchronization.Files.First(f => f.MultimediaCode == file.MultimediaCode).IsSynchronized = true;
                    //                    Clients.Group(Broadcaster.DevicesGroupId).StatusChange(device);
                    //                }
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            result = false;
                    //            _logger.LogError(125, ex, $"Sincronización > Ocurrio un error sincronizando el archivo \"{file.Filename}\" de la extracción \"{extraction.ExtractionCode}\" del dispositivo \"{device.DeviceCode}\". Detalle: " + ex.Message);
                    //        }
                    //    }
                    //    SQLiteService.CloseConnection();
                    //}
                }
            }
            catch (Exception ex)
            {
                result = false;
                _logger.LogError(125, ex, $"Sincronización > Ocurrio un error de sincronización de encuestas. Detalle: " + ex.Message);
            }

            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost, Route("sync-files")]
        public async Task<IActionResult> SyncFiles([FromBody] Device device)
        {
            var result = true;
            SQLiteService SQLiteService = new SQLiteService();
            try
            {
                //_logger.LogInformation("Sincronización de archivos > device.Synchronizations: " + JsonConvert.SerializeObject(device.Synchronizations, Formatting.Indented, new JsonSerializerSettings()
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //}));
                foreach (var synchronization in device.Synchronizations)
                {
                    //_logger.LogInformation("Sincronización de archivos > synchronization: " + JsonConvert.SerializeObject(synchronization, Formatting.Indented, new JsonSerializerSettings()
                    //{
                    //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    //}));
                    LogDto logDto = new LogDto();
                    logDto.CodVersion = synchronization.VersionCode;
                    logDto.Modalidad = "Extractor";
                    logDto.Dispositivo = device.DeviceCode;
                    string hostName = await NetworkService.Instance.GetHostName();
                    logDto.DispositivoExtraccion = hostName ?? "Desconocido";

                    List<Extraction> extractions = _extractionRepository.GetExtractionsByDeviceAndVersionCode(device.DeviceCode, synchronization.VersionCode);
                    //_logger.LogInformation("Sincronización de archivos > extractions: " + JsonConvert.SerializeObject(extractions, Formatting.Indented, new JsonSerializerSettings()
                    //{
                    //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    //}));

                    foreach (var extraction in extractions)
                    {
                        SQLiteService.OpenConnection(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode);
                        logDto.DetalleLog += SQLiteService.GetSurveySynchronizationLog(synchronization.VersionCode) + "|";
                        logDto.DetalleMultimediaLog += SQLiteService.GetMultimediaSynchronizationLog(synchronization.VersionCode) + "|";
                        SQLiteService.CloseConnection();
                    }

                    if (logDto.DetalleLog.EndsWith("|"))
                    {
                        logDto.DetalleLog = logDto.DetalleLog.Remove(logDto.DetalleLog.Length - 1);
                    }

                    if (logDto.DetalleMultimediaLog.EndsWith("|"))
                    {
                        logDto.DetalleMultimediaLog = logDto.DetalleMultimediaLog.Remove(logDto.DetalleMultimediaLog.Length - 1);
                    }

                    bool uploadSynchronizationLogResult = false;
                    if (!logDto.DetalleLog.Equals("") || !logDto.DetalleMultimediaLog.Equals(""))
                    {
                        uploadSynchronizationLogResult = await SynchronizationService.GetInstance(_settingRepository, device.Environment).UploadSynchronizationLog(logDto);
                    }

                    if (!uploadSynchronizationLogResult)
                    {
                        continue;
                    }

                    foreach (var extraction in extractions)
                    {
                        //_logger.LogInformation("Sincronización de archivos > extraction: " + JsonConvert.SerializeObject(extraction, Formatting.Indented, new JsonSerializerSettings()
                        //{
                        //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        //}));
                        SQLiteService.OpenConnection(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode);
                        //List<File> files = _fileRepository.GetNonSynchronizedFilesByExtractionCode(extraction.ExtractionCode);
                        List<File> files = _fileRepository.GetNonSynchronizedFilesByExtractionId(extraction.ExtractionCode, extraction.Id);
                        //_logger.LogInformation("Sincronización de archivos > files: " + JsonConvert.SerializeObject(files, Formatting.Indented, new JsonSerializerSettings()
                        //{
                        //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        //}));
                        foreach (var file in files)
                        {
                            //_logger.LogInformation("Sincronización de archivos > file: " + JsonConvert.SerializeObject(file, Formatting.Indented, new JsonSerializerSettings()
                            //{
                            //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            //}));
                            try
                            {
                                //_logger.LogInformation("Sincronización de archivos > Iniciando subida...");
                                bool uploadFileResult = await SynchronizationService.GetInstance(_settingRepository, device.Environment).UploadFile(_extractedFilesBaseDirectory, device.DeviceCode, extraction.ExtractionCode, extraction.FormCode, extraction.VersionCode, file, _logger);
                                //_logger.LogInformation("Sincronización de archivos > Termino subida");
                                if (uploadFileResult)
                                {
                                    //_logger.LogInformation("Sincronización de archivos > uploadFileResult es verdadero");
                                    _fileRepository.SetIsSynchronizedByMultimediaCode(file.MultimediaCode, true);
                                    //_logger.LogInformation("Sincronización de archivos > Paso SetIsSynchronizedByMultimediaCode");
                                    int editFileResult = _fileRepository.Save();
                                    //_logger.LogInformation("Sincronización de archivos > Paso Save");
                                    int setIsSynchronizedMultimediaResult =
                                        SQLiteService.UpdateIsSynchronizedMultimedia(file.MultimediaCode, true);
                                    //_logger.LogInformation("Sincronización de archivos > Paso setIsSynchronizedMultimediaResult");
                                    if (editFileResult > 0 && setIsSynchronizedMultimediaResult > 0)
                                    {
                                        //_logger.LogInformation("Sincronización de archivos > editFileResult es mayor que cero y setIsSynchronizedMultimediaResult es mayor que cero");
                                        synchronization.UploadedMultimediaQuantity++;
                                        synchronization.Files.First(f => f.MultimediaCode == file.MultimediaCode)
                                            .IsSynchronized = true;
                                        _hubContext.Clients.Group(Broadcaster.DevicesGroupId).StatusChange(device);
                                        //_logger.LogInformation("Sincronización de archivos > se envio StatusChange");
                                    }
                                }
                                else
                                {
                                    _logger.LogInformation("Sincronización de archivos > uploadFileResult es falso");
                                    _logger.LogError($"Sincronización > Ocurrio un error sincronizando el archivo \"{file.Filename}\" de la extracción \"{extraction.ExtractionCode}\" del dispositivo \"{device.DeviceCode}\".");
                                }
                            }
                            catch (Exception ex)
                            {
                                result = false;
                                _logger.LogError(125, ex, $"Sincronización > Ocurrio un error sincronizando el archivo \"{file.Filename}\" de la extracción \"{extraction.ExtractionCode}\" del dispositivo \"{device.DeviceCode}\". Detalle: " + ex.Message);
                            }
                        }
                        SQLiteService.CloseConnection();
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                _logger.LogError(125, ex, $"Sincronización > Ocurrio un error de sincronización de archivos. Detalle: {ex.Message}");
            }

            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete, Route("devices/remove-all")]
        public async Task<IActionResult> RemoveAll()
        {
            try
            {
                _deviceRepository.RemoveAll();
                _deviceRepository.Save();
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
