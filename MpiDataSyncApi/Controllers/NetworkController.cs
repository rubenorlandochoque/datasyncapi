﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MpiDataSyncApi.Services;

namespace MpiDataSyncApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NetworkController : ControllerBase
    {
        [HttpGet(),Route("LocalIpAddress")]
        public async Task<ActionResult<string>> GetLocalIpAddress()
        {
            IPAddress localIpAddress = await NetworkService.Instance.GetLocalIpAddress();
            return localIpAddress != null ? localIpAddress.ToString() : "Desconocida";
        }
    }
}