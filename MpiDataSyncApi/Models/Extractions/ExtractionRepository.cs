﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MpiDataSyncApi.Data;
using MpiDataSyncApi.Models.Devices;

namespace MpiDataSyncApi.Models.Extractions
{
    public class ExtractionRepository : IExtractionRepository
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed = false;

        public ExtractionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Extraction> GetExtractions()
        {
            return _context.Extractions.ToList();
        }

        public List<Extraction> GetExtractionsByDeviceId(long deviceId)
        {
            return _context.Extractions.Where(e => e.DeviceId == deviceId).ToList();
        }

        public List<Extraction> GetExtractionsByDeviceCode(string deviceCode)
        {
            Device device = _context.Devices.FirstOrDefault(d => d.DeviceCode == deviceCode);
            long deviceId = device.Id;
            return GetExtractionsByDeviceId(deviceId);
        }

        public List<Extraction> GetExtractionsByVersionCode(string versionCode)
        {
            return _context.Extractions.Where(e => e.VersionCode == versionCode && !e.IsSynchronized).ToList();
        }

        public List<Extraction> GetExtractionsByDeviceAndVersionCode(string deviceCode, string versionCode)
        {
            Device device = _context.Devices.FirstOrDefault(d => d.DeviceCode == deviceCode);
            long deviceId = device?.Id ?? -1;
            return _context.Extractions.Where(e => e.VersionCode == versionCode && e.DeviceId == deviceId && !e.IsSynchronized && (_context.Surveys.Any(s => s.ExtractionId == e.Id && !s.IsSynchronized) || _context.Files.Any(f => f.ExtractionId == e.Id && !f.IsSynchronized))).ToList();
        }

        public Extraction GetExtractionById(long id)
        {
            return _context.Extractions.FirstOrDefault(e => e.Id == id);
        }

        public Extraction GetExtractionByExtractionCode(string extractionCode)
        {
            return _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
        }

        public void Add(Extraction extraction)
        {
            _context.Extractions.Add(extraction);
        }

        public void Edit(Extraction extraction)
        {
            _context.Entry(extraction).State = EntityState.Modified;
        }

        public void RemoveById(long id)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.Id == id);
            _context.Extractions.Remove(extraction);
        }

        public void RemoveByExtractionCode(string extractionCode)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
            _context.Extractions.Remove(extraction);
        }

        public long GetIdByCode(string extractionCode)
        {
            long id = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode)?.Id ?? -1;
            return id;
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
