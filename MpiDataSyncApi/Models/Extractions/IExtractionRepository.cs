﻿using System;
using System.Collections.Generic;

namespace MpiDataSyncApi.Models.Extractions
{
    public interface IExtractionRepository : IDisposable
    {
        List<Extraction> GetExtractions();

        List<Extraction> GetExtractionsByDeviceId(long deviceId);

        List<Extraction> GetExtractionsByDeviceCode(string deviceCode);

        List<Extraction> GetExtractionsByVersionCode(string versionCode);

        List<Extraction> GetExtractionsByDeviceAndVersionCode(string deviceCode, string versionCode);

        Extraction GetExtractionById(long id);

        Extraction GetExtractionByExtractionCode(string extractionCode);

        void Add(Extraction file);

        void Edit(Extraction file);

        void RemoveById(long id);

        void RemoveByExtractionCode(string extractionCode);

        long GetIdByCode(string extractionCode);

        int Save();
    }
}
