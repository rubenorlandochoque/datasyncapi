﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MpiDataSyncApi.Models.Devices;
using MpiDataSyncApi.Models.Files;
using MpiDataSyncApi.Models.Surveys;
using Newtonsoft.Json;

namespace MpiDataSyncApi.Models.Extractions
{
    [Table("Extractions")]
    public class Extraction : IAuditable
    {
        public Extraction()
        {
            IsSynchronized = false;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        [MaxLength(127)]
        public string ExtractionCode { get; set; }

        [MaxLength(127)]
        public string FormCode { get; set; }

        [MaxLength(127)]
        public string FormName { get; set; }

        [MaxLength(127)]
        public string VersionCode { get; set; }

        public int SurveysQuantity { get; set; }

        public int NewsQuantity { get; set; }

        public int InterestPointsQuantity { get; set; }

        public int MultimediaQuantity { get; set; }

        public bool IsSynchronized { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }

        public virtual long DeviceId { get; set; }

        
        public virtual ICollection<File> Files { get; set; }

        
        public virtual ICollection<Survey> Surveys { get; set; }

        [NotMapped]
        public bool IsExtractionEnabled { get; set; }

        #region Auditable fields

        public DateTime CreatedDate { get; set; }

        [MaxLength(127)]
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [MaxLength(127)]
        public string UpdatedBy { get; set; }

        #endregion
    }
}
