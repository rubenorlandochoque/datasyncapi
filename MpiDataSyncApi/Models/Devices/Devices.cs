﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MpiDataSyncApi.Models.Extractions;
using MpiDataSyncApi.Models.Synchronizations;
using Newtonsoft.Json;

namespace MpiDataSyncApi.Models.Devices
{
    [Table("Devices")]
    public class Device : IAuditable
    {
        public Device()
        {
            Extractions = new List<Extraction>();
            Synchronizations = new List<Synchronization>();
        }

        public long Id { get; set; }

        [MaxLength(127)]
        public string DeviceCode { get; set; }

        [MaxLength(127)]
        public string Name { get; set; }

        [NotMapped]
        [MaxLength(127)]
        public string ConnectionId { get; set; }

        [NotMapped]
        public string ExtractionCode { get; set; }

        [NotMapped]
        public Status Status { get; set; }

        [NotMapped]
        public Progress Progress { get; set; }

        //[NotMapped]
        //public List<FormDetail> ToExtraction { get; set; }

        public virtual ICollection<Extraction> Extractions { get; set; }

        [NotMapped]
        public ICollection<Synchronization> Synchronizations { get; set; }

        [NotMapped]
        public string Environment { get; set; }

        #region Auditable fields

        public DateTime CreatedDate { get; set; }

        [MaxLength(127)]
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [MaxLength(127)]
        public string UpdatedBy { get; set; }

        #endregion
    }
}
