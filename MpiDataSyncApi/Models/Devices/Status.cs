﻿namespace MpiDataSyncApi.Models.Devices
{
    public class Status
    {
        public bool IsReadyToExtraction { get; set; }

        public bool IsReadyToSync { get; set; }
    }
}