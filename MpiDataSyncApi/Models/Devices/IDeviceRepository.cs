﻿using System;
using System.Collections.Generic;

namespace MpiDataSyncApi.Models.Devices
{
    public interface IDeviceRepository : IDisposable
    {
        int GetDevicesQuantity();

        List<Device> GetDevices();

        List<Device> GetDevices(int currentPageNumber, int numberOfRecordsPerPage);

        void GetDevicesInfo();

        Device GetDeviceByDeviceCode(string deviceCode, bool withInclude);

        void Add(Device device);

        void Edit(Device device);

        void Remove(string deviceId);

        void RemoveAll();

        int Save();
    }
}