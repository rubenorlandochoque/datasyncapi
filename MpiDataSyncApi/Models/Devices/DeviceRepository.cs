﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MpiDataSyncApi.Data;

namespace MpiDataSyncApi.Models.Devices
{
    public class DeviceRepository : IDeviceRepository
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed = false;

        public DeviceRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public int GetDevicesQuantity()
        {
            return _context.Devices.Count();
        }

        public List<Device> GetDevices()
        {
            return _context.Devices
                    .Include(d => d.Extractions)
                        .ThenInclude(f => f.Files)
                    .Include(d => d.Extractions)
                        .ThenInclude(s => s.Surveys)
                    .ToList();
        }

        public List<Device> GetDevices(int currentPageNumber, int numberOfRecordsPerPage)
        {
            return _context.Devices
                .Include(d => d.Extractions)
                .ThenInclude(f => f.Files)
                .Include(d => d.Extractions)
                .ThenInclude(s => s.Surveys)
                .Skip((currentPageNumber - 1) * numberOfRecordsPerPage)
                .Take(numberOfRecordsPerPage)
                .ToList();
        }

        public void GetDevicesInfo()
        {
            var devicesInfo = from d in _context.Devices
                              join e in _context.Extractions on d.Id equals e.DeviceId into t1
                              from t2 in t1.DefaultIfEmpty()
                              group t2 by d.Id
                into grouped
                              select new { Id = grouped.Key, Count = grouped.Count(t => t.Id != null) };
        }

        public Device GetDeviceByDeviceCode(string deviceCode, bool withInclude)
        {
            //List<Device> devices = GetDevices();
            //return devices.FirstOrDefault(d => d.DeviceCode == deviceCode);

            if (withInclude)
            {
                //List<Device> devices = GetDevices();
                //return devices.FirstOrDefault(d => d.DeviceCode == deviceCode);
                return _context.Devices.Where(d => d.DeviceCode == deviceCode)
                    .Include(d => d.Extractions)
                        .ThenInclude(e => e.Files)
                    .Include(d => d.Extractions)
                        .ThenInclude(e => e.Surveys)
                    .First();
            }
            return _context.Devices.FirstOrDefault(d => d.DeviceCode == deviceCode);
        }

        public void Add(Device device)
        {
            _context.Devices.Add(device);
        }

        public void Edit(Device device)
        {
            _context.Entry(device).State = EntityState.Modified;
        }

        public void Remove(string deviceCode)
        {
            Device device = _context.Devices.FirstOrDefault(d => d.DeviceCode == deviceCode);
            _context.Devices.Remove(device);
        }

        public void RemoveAll()
        {
            var devices = _context.Devices;
            _context.Devices.RemoveRange(devices);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
