﻿namespace MpiDataSyncApi.Models.Devices
{
    public class Progress
    {
        public double MinimumInBytes { get; set; }

        public double MaximumInBytes { get; set; }

        public double ValueInBytes { get; set; }

        public int ValueInPercentage { get; set; }

        public bool IsExtracting { get; set; }

        public bool IsExtractionComplete { get; set; }

        public bool IsExtractionSuccess { get; set; }

        public bool IsSynchronizing { get; set; }

        public bool IsSynchronizationComplete { get; set; }

        public bool IsSynchronizationSuccess { get; set; }
    }
}