﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MpiDataSyncApi.Models
{
    public interface IAuditable
    {
        DateTime CreatedDate { get; set; }

        [MaxLength(100)]
        string CreatedBy { get; set; }

        DateTime UpdatedDate { get; set; }

        [MaxLength(100)]
        string UpdatedBy { get; set; }
    }
}