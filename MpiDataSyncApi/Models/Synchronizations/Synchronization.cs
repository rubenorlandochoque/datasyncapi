﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MpiDataSyncApi.Models.Files;
using MpiDataSyncApi.Models.Surveys;
using Newtonsoft.Json;

namespace MpiDataSyncApi.Models.Synchronizations
{
    public class Synchronization
    {
        public Synchronization()
        {
            Files = new List<File>();
            Surveys = new List<Survey>();
        }

        [MaxLength(127)]
        public string SynchronizationCode { get; set; }

        [MaxLength(127)]
        public string FormCode { get; set; }

        [MaxLength(127)]
        public string FormName { get; set; }

        [MaxLength(127)]
        public string VersionCode { get; set; }

        public int SurveysQuantity { get; set; }

        public int NewsQuantity { get; set; }

        public int InterestPointsQuantity { get; set; }

        public int MultimediaQuantity { get; set; }

        public int UploadedSurveysQuantity { get; set; }

        public int UploadedNewsQuantity { get; set; }

        public int UploadedInterestPointsQuantity { get; set; }

        public int UploadedMultimediaQuantity { get; set; }

        [NotMapped]
        public virtual ICollection<File> Files { get; set; }

        [NotMapped]
        public virtual ICollection<Survey> Surveys { get; set; }
    }
}
