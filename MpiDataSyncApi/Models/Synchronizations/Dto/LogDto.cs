﻿
namespace MpiDataSyncApi.Models.Synchronizations.Dto
{
    public class LogDto
    {
        public LogDto()
        {
            CodVersion = "";
            DetalleLog = "";
            DetalleMultimediaLog = "";
            Modalidad = "";
            Dispositivo = "";
            DispositivoExtraccion = "";
        }

        public string CodVersion { get; set; }

        public string DetalleLog { get; set; } //CodEncuesta1,FechaInicio1,Usuario1,Estado1|CodEncuesta,FechaInicio,Usuario,Estado

        public string DetalleMultimediaLog { get; set; } //CodMult,Archivo,CodEncuesta6,tipoVideo|

        public string Modalidad { get; set; } //

        public string Dispositivo { get; set; }

        public string DispositivoExtraccion { get; set; }
    }
}
