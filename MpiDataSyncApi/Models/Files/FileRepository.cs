﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MpiDataSyncApi.Data;
using MpiDataSyncApi.Models.Extractions;

namespace MpiDataSyncApi.Models.Files
{
    public class FileRepository : IFileRepository
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed = false;

        public FileRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<File> GetFiles()
        {
            return _context.Files.ToList();
        }

        public List<File> GetFilesByExtractionId(long id)
        {
            return _context.Files.Where(f => f.ExtractionId == id).ToList();
        }

        public List<File> GetFilesByExtractionCode(string extractionCode)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
            long extractionId = -1;
            if (extraction != null)
            {
                extractionId = extraction.Id;
            }
            return GetFilesByExtractionId(extractionId);
        }

        public List<File> GetNonSynchronizedFilesByExtractionCode(string extractionCode)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
            long extractionId = -1;
            if (extraction != null)
            {
                extractionId = extraction.Id;
            }
            return _context.Files.Where(f => f.ExtractionId == extractionId && !f.IsSynchronized).ToList();
        }

        public List<File> GetNonSynchronizedFilesByExtractionId(string extractionCode, long extractionId)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
            if (extraction == null)
            {
                extractionId = -1;
            }
            return _context.Files.Where(f => f.ExtractionId == extractionId && !f.IsSynchronized).ToList();
        }

        public File GetFileById(long id)
        {
            return _context.Files.FirstOrDefault(f => f.Id == id);
        }

        public File GetFileByFilename(string filename)
        {
            return _context.Files.FirstOrDefault(f => f.Filename == filename);
        }

        public void Add(File file)
        {
            _context.Files.Add(file);
        }

        public void Edit(File file)
        {
            _context.Entry(file).State = EntityState.Modified;
        }

        public void SetIsSynchronizedByMultimediaCode(string multimediaCode, bool isSynchronized)
        {
            var files = _context.Files.Where(f => f.MultimediaCode == multimediaCode).ToList();
            foreach (var file in files)
            {
                file.IsSynchronized = isSynchronized;
                _context.Entry(file).State = EntityState.Modified;
            }
        }

        public void RemoveById(long id)
        {
            File file = _context.Files.FirstOrDefault(f => f.Id == id);
            _context.Files.Remove(file);
        }

        public void RemoveByFilename(string filename)
        {
            File file = _context.Files.FirstOrDefault(f => f.Filename == filename);
            _context.Files.Remove(file);
        }

        public void RemoveByExtractionId(long extractionId)
        {
            _context.Files.RemoveRange(_context.Files.Where(f => f.ExtractionId == extractionId));
        }

        public void RemoveByExtractionCodeAndSurveyCode(string surveyCode)  //e.Extraction.ExtractionCode == extractionCode
        {
            var entities =
                _context.Files.Where(
                    e =>
                        !e.IsSynchronized && e.SurveyCode == surveyCode);
            _context.Files.RemoveRange(entities);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
