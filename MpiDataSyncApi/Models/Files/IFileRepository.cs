﻿using System;
using System.Collections.Generic;

namespace MpiDataSyncApi.Models.Files
{
    public interface IFileRepository : IDisposable
    {
        List<File> GetFiles();

        List<File> GetFilesByExtractionId(long id);

        List<File> GetFilesByExtractionCode(string extractionCode);

        List<File> GetNonSynchronizedFilesByExtractionCode(string extractionCode);

        List<File> GetNonSynchronizedFilesByExtractionId(string extractionCode, long extractionId);

        File GetFileById(long id);

        File GetFileByFilename(string filename);

        void Add(File file);

        void Edit(File file);

        void SetIsSynchronizedByMultimediaCode(string multimediaCode, bool isSynchronized);

        void RemoveById(long id);

        void RemoveByFilename(string filename);

        void RemoveByExtractionId(long extractionId);

        void RemoveByExtractionCodeAndSurveyCode(string surveyCode);

        int Save();
    }
}
