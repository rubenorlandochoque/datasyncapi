﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MpiDataSyncApi.Models.Extractions;
using Newtonsoft.Json;

namespace MpiDataSyncApi.Models.Files
{
    [Table("Files")]
    public class File : IAuditable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        [MaxLength(127)]
        public string MultimediaCode { get; set; }

        [MaxLength(1023)]
        public string Filename { get; set; }

        [MaxLength(127)]
        public string EntityCode { get; set; }

        [MaxLength(127)]
        public string ObjectCode { get; set; }

        [MaxLength(127)]
        public string SurveyCode { get; set; }

        [MaxLength(127)]
        public string RegisterDate { get; set; }

        [MaxLength(127)]
        public string FileClasificationCode { get; set; }

        [MaxLength(127)]
        public string QuestionSectionCode { get; set; }

        [MaxLength(127)]
        public string GroupCode { get; set; }

        [MaxLength(127)]
        public string MultimediaTypeCode { get; set; }

        [MaxLength(127)]
        public string ObservationTypeCode { get; set; }

        public int Duration { get; set; }

        public bool IsExtracted { get; set; }

        public bool IsSynchronized { get; set; }

        [JsonIgnore]
        [ForeignKey("ExtractionId")]
        public virtual Extraction Extraction { get; set; }

        public virtual long ExtractionId { get; set; }

        #region Auditable fields

        public DateTime CreatedDate { get; set; }

        [MaxLength(100)]
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [MaxLength(100)]
        public string UpdatedBy { get; set; }

        #endregion
    }
}
