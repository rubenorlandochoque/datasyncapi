﻿using System;
using System.Collections.Generic;

namespace MpiDataSyncApi.Models.Settings
{
    public interface ISettingRepository : IDisposable
    {
        List<Setting> GetSettings();

        List<Setting> GetSettingsByKey(string key);

        Setting GetDefaultSettingByKey(string key);

        Setting GetSettingById(int id);

        Setting GetByKeyAndName(string key, string name);

        void Add(Setting setting);

        void Edit(Setting setting);

        void Remove(int id);

        int Save();
    }
}