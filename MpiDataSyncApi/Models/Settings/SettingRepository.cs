﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MpiDataSyncApi.Data;

namespace MpiDataSyncApi.Models.Settings
{
    public class SettingRepository : ISettingRepository
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed = false;

        public SettingRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Setting> GetSettings()
        {
            return _context.Settings.ToList();
        }

        public List<Setting> GetSettingsByKey(string key)
        {
            return _context.Settings.Where(s => s.Key == key).ToList();
        }

        public Setting GetDefaultSettingByKey(string key)
        {
            return _context.Settings.FirstOrDefault(s => s.Key == key && s.IsDefault);
        }

        public Setting GetSettingById(int id)
        {
            return _context.Settings.FirstOrDefault(s => s.Id == id);
        }

        public Setting GetByKeyAndName(string key, string name)
        {
            return _context.Settings.FirstOrDefault(s => s.Key == key && s.Name.Equals(name));
        }

        public void Add(Setting setting)
        {
            _context.Settings.Add(setting);
        }

        public void Edit(Setting setting)
        {
            _context.Entry(setting).State = EntityState.Modified;
        }

        public void Remove(int id)
        {
            Setting setting = _context.Settings.FirstOrDefault(s => s.Id == id);
            _context.Settings.Remove(setting);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}