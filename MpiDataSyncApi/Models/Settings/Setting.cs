﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MpiDataSyncApi.Models.Settings
{
    [Table("Settings")]
    public class Setting : IAuditable
    {
        public Setting()
        {

        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [MaxLength(127)]
        public string Key { get; set; }

        [MaxLength(1023)]
        public string Value { get; set; }

        [MaxLength(127)]
        public string Name { get; set; }

        [MaxLength(127)]
        public string DisplayName { get; set; }

        public bool IsDefault { get; set; }

        #region Auditable fields

        public DateTime CreatedDate { get; set; }

        [MaxLength(127)]
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [MaxLength(127)]
        public string UpdatedBy { get; set; }

        #endregion
    }
}
