﻿using System;
using System.Collections.Generic;

namespace MpiDataSyncApi.Models.Surveys
{
    public interface ISurveyRepository : IDisposable
    {
        List<Survey> GetSurveysByExtractionCode(string extractionCode);

        List<Survey> GetNonSynchronizedSurveysByExtractionCode(string extractionCode);

        List<Survey> GetNonSynchronizedSurveysByExtractionId(long extractionId);

        void Edit(Survey survey);

        void SetIsSynchronizedBySurveyCode(string surveyCode, bool isSynchronized);

        void RemoveByExtractionId(long extractionId);

        void RemoveByExtractionCodeAndSurveyCode(string surveyCode);

        int Save();
    }
}
