﻿namespace MpiDataSyncApi.Models.Surveys.Dto
{
    public class UploadSurveyDto
    {
        public string SurveyCode { get; set; }

        public string VersionCode { get; set; }

        public string ApplicationCode { get; set; }

        public string Header { get; set; }

        public string Answer { get; set; }

        public string Persons { get; set; }

        public string Households { get; set; }

        public string Location { get; set; }

        public string Families { get; set; }

        public string FamiliesVisits { get; set; }

        public string PersonsFamilies { get; set; }

        public string Markers { get; set; }

        public string Multimedia { get; set; }

        public string MultimediaObservation { get; set; }

        public string Relationship { get; set; }

        public string Status { get; set; }

        public string SurveyErrors { get; set; }

        public string ExtractionCode { get; set; }
    }
}
