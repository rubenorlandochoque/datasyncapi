﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MpiDataSyncApi.Data;
using MpiDataSyncApi.Models.Extractions;

namespace MpiDataSyncApi.Models.Surveys
{
    public class SurveyRepository : ISurveyRepository
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed = false;

        public SurveyRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Survey> GetSurveysByExtractionCode(string extractionCode)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
            long extractionId = extraction.Id;
            return _context.Surveys.Where(s => s.ExtractionId == extractionId).ToList();
        }

        public List<Survey> GetNonSynchronizedSurveysByExtractionCode(string extractionCode)
        {
            Extraction extraction = _context.Extractions.FirstOrDefault(e => e.ExtractionCode == extractionCode);
            long extractionId = extraction.Id;
            return _context.Surveys.Where(s => s.ExtractionId == extractionId && !s.IsSynchronized).ToList();
        }

        public List<Survey> GetNonSynchronizedSurveysByExtractionId(long extractionId)
        {
            return _context.Surveys.Where(s => s.ExtractionId == extractionId && !s.IsSynchronized).ToList();
        }

        public void Edit(Survey survey)
        {
            _context.Entry(survey).State = EntityState.Modified;
        }

        public void SetIsSynchronizedBySurveyCode(string surveyCode, bool isSynchronized)
        {
            var surveys = _context.Surveys.Where(s => s.SurveyCode == surveyCode).ToList();
            foreach (var survey in surveys)
            {
                survey.IsSynchronized = isSynchronized;
                _context.Entry(survey).State = EntityState.Modified;
            }
        }

        public void RemoveByExtractionId(long extractionId)
        {
            _context.Surveys.RemoveRange(_context.Surveys.Where(s => s.ExtractionId == extractionId));
        }

        public void RemoveByExtractionCodeAndSurveyCode(string surveyCode)  //e.Extraction.ExtractionCode == extractionCode
        {
            var entities =
                _context.Surveys.Where(
                    e =>
                        !e.IsSynchronized && e.SurveyCode == surveyCode);
            _context.Surveys.RemoveRange(entities);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
