﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MpiDataSyncApi.Models.Extractions;
using Newtonsoft.Json;

namespace MpiDataSyncApi.Models.Surveys
{
    [Table("Surveys")]
    public class Survey : IAuditable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        public string SurveyCode { get; set; }

        public string StatusCode { get; set; }

        public bool IsSynchronized { get; set; }

        [JsonIgnore]
        [ForeignKey("ExtractionId")]
        public virtual Extraction Extraction { get; set; }

        public virtual long ExtractionId { get; set; }

        #region Auditable fields

        public DateTime CreatedDate { get; set; }

        [MaxLength(100)]
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [MaxLength(100)]
        public string UpdatedBy { get; set; }

        #endregion
    }
}
