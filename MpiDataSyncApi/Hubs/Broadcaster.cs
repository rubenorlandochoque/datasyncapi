﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MpiDataSyncApi.Hubs;
using MpiDataSyncApi.Models.Devices;


namespace MpiDataSyncApi.Hubs
{
    public class Broadcaster : Hub<IBroadcaster>
    {
        public static readonly string DevicesGroupId = "DevicesGroupId";

        public override Task OnConnectedAsync()
        {
            // Add your own code here.
            // For example: in a chat application, record the association between
            // the current connection ID and user name, and mark the user as online.
            // After the code in this method completes, the client is informed that
            // the connection is established; for example, in a JavaScript client,
            // the start().done callback is executed.
            Clients.Client(Context.ConnectionId).SetConnectionId(Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            // Add your own code here.
            // For example: in a chat application, mark the user as offline, 
            // delete the association between the current connection id and user name.
            Clients.Group(DevicesGroupId).ILeft(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }

        // Server side methods called from client
        public Task Subscribe(string groupId)
        {
            Groups.AddToGroupAsync(Context.ConnectionId, groupId);
            return Clients.Client(Context.ConnectionId).SubscribeDone(Context.ConnectionId);
        }

        public Task Unsubscribe(string groupId)
        {
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, groupId);
        }

        public Task ImReady(Device device)
        {
            return Clients.Group(DevicesGroupId).ImReady(device);
        }

        public Task StartExtraction(Device device)
        {
            return Clients.Client(device.ConnectionId).StartExtraction();
        }

        public Task StatusChange(Device device)
        {
            return Clients.All.StatusChange(device);
        }

        public Task TakeAssistance()
        {
            return Clients.Others.TakeAssistance();
        }

        public Task ILeft(string connectionId)
        {
            return Clients.Group(DevicesGroupId).ILeft(connectionId);
        }

        public Task ExtractionComplete(Device device)
        {
            return Clients.Group(DevicesGroupId).ExtractionComplete(device);
        }
    }
}
