﻿using System;
using System.Threading.Tasks;
using MpiDataSyncApi.Models.Devices;

namespace MpiDataSyncApi.Hubs
{
    public interface IBroadcaster
    {
        Task SetConnectionId(string connectionId);

        Task SubscribeDone(string connectionId);

        Task ImReady(Device device);

        Task ILeft(string connectionId);

        Task StartExtraction();

        Task StatusChange(Device device);

        Task TakeAssistance();

        Task ExtractionComplete(Device device);
    }
}
