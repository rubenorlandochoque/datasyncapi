﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace MpiDataSyncApi.Helpers
{
    public static class CompressionHelper
    {
        /// <summary>
        /// Compress string, Base64 and returns an compress string.
        /// </summary>
        /// <param name="decompressedString"></param>
        /// <returns></returns>
        public static string Compress(string decompressedString)
        {
            var compressedStream = new MemoryStream();
            var uncompressedStream = new MemoryStream(Encoding.UTF8.GetBytes(decompressedString));

            using (var compressorStream = new System.IO.Compression.DeflateStream(compressedStream, System.IO.Compression.CompressionMode.Compress, true))
            {
                uncompressedStream.CopyTo(compressorStream);
            }

            return Convert.ToBase64String(compressedStream.ToArray());
        }

        /// <summary>
        /// Decompresses a deflate compressed, Base64 encoded string and returns an uncompressed string.
        /// </summary>
        /// <param name="compressedString">String to decompress.</param>
        public static string Decompress(string compressedString)
        {
            var decompressedStream = new MemoryStream();
            var compressedStream = new MemoryStream(Convert.FromBase64String(compressedString));

            using (var decompressorStream = new System.IO.Compression.DeflateStream(compressedStream, System.IO.Compression.CompressionMode.Decompress))
            {
                decompressorStream.CopyTo(decompressedStream);
            }

            return Encoding.UTF8.GetString(decompressedStream.ToArray());
        }

        /// <summary>
        /// Compress string, Base64 and returns an compress string.
        /// </summary>
        /// <param name="decompressedString"></param>
        /// <returns></returns>
        public static string GzipCompress(string decompressedString)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(decompressedString);
            MemoryStream ms = new MemoryStream();
            using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            return Convert.ToBase64String(compressed);
        }

        /// <summary>
        /// Decompresses a deflate compressed, Base64 encoded string and returns an uncompressed string.
        /// </summary>
        /// <param name="compressedString">String to decompress.</param>
        public static string GzipDecompress(string compressedString)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedString);
            using (GZipStream stream = new GZipStream(new MemoryStream(gZipBuffer), CompressionMode.Decompress))
            {
                const int size = 32;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return Encoding.UTF8.GetString(memory.ToArray());
                }
            }
        }

        /// <summary>e
        /// Compress uncompressedString, Base64 and returns an compressed string.
        /// </summary>
        /// <param name="decompressedString"></param>
        /// <returns></returns>
        public static string DeflaterCompress(string decompressedString)
        {
            byte[] toCompress = Encoding.UTF8.GetBytes(decompressedString);
            using (MemoryStream compressedStream = new MemoryStream())
            {
                compressedStream.Position = 0;
                using (DeflateStream deflater = new DeflateStream(compressedStream, CompressionMode.Compress))
                {
                    deflater.Write(toCompress, 0, toCompress.Length);
                }

                return Convert.ToBase64String(compressedStream.ToArray());
            }
        }

        /// <summary>
        /// Decompresses a deflate compressed, Base64 encoded string and returns an uncompressed string.
        /// </summary>
        /// <param name="compressedString">String to decompress.</param>
        public static string DeflaterDecompress(string compressedString)
        {
            byte[] toDecompress = Convert.FromBase64String(compressedString);
            using (MemoryStream decompressedStream = new MemoryStream())
            {
                using (MemoryStream compressedStream = new MemoryStream(toDecompress))
                {
                    using (DeflateStream deflater = new DeflateStream(compressedStream, CompressionMode.Decompress))
                    {
                        int c = 0;
                        while ((c = deflater.ReadByte()) != -1)
                        {
                            decompressedStream.WriteByte((byte)c);
                        }
                    }
                }
                return Encoding.UTF8.GetString(decompressedStream.ToArray(), 0, decompressedStream.ToArray().Length);
            }
        }

    }
}